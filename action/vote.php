<?php
	include_once('../database/votes.php');

	function vote($userid, $pollid, $answerid){
        insertVote($userid, $pollid, $answerid);
    }

    if (isset($_POST['userid'])) {
        vote($_POST['userid'], $_POST['pollid'], $_POST['answerid']);
    }

?>