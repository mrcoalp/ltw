<?php
	function checkImg($imgFile){
		$target_dir="./images";
		$imgpath = $target_dir . basename($imgFile["name"]);
		$uploadOk = 1;
		$imageFileType = pathinfo($imgpath,PATHINFO_EXTENSION);
		// Check if image file is a actual image or fake image
		$check = getimagesize($imgFile["tmp_name"]);
		if($check !== false) {
			echo "<h3>File is an image - " . $check["mime"] . ".</h3>";
			$uploadOk = 1;
		} 
		else {
			echo "<h3>File is not an image.</h3>";
			$uploadOk = 0;
		}

		// Check if file already exists
		if (file_exists($imgpath)) {
			echo "<h3>Sorry, file already exists.</h3>";
			$uploadOk = 0;
		}

		// Check file size
		if ($imgFile["size"] > 2000000) {
			echo "<h3>Sorry, your file is too large.</h3>";
			$uploadOk = 0;
		}

		// Allow certain file formats
		if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
		&& $imageFileType != "gif" ) {
		echo "<h3>Sorry, only JPG, JPEG, PNG & GIF files are allowed.</h3>";
		$uploadOk = 0;
		
		}
		
		return $uploadOk;
	}

?>