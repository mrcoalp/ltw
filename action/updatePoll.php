<?php
	session_start();

	include_once('../database/answers.php');

	if (isset($_POST['myid'])) {
		$i = 0;
		if(isset($_POST["mytext"])){    
		    foreach($_POST["mytext"] as $key => $text_field){
		    	$id_field = $_POST["myid"][$i];
		    	if(!empty($text_field)){
		        	editAnswer($id_field, $text_field);
		    	}
		    	$i++;
		    }
		}
    }

    header("Location: ../pages/mypolls.php");
?>