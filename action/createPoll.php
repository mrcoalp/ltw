<?php
	session_start();

	include_once('../database/polls.php');
	include_once('../database/answers.php');
	include_once('upload.php');

	error_reporting(0);

	$target_dir="../images/";
	$title = $_POST['question'];
	$userid = $_SESSION['userid'];
	$imgpath = $target_dir . basename($_FILES["file"]["name"]);
	$uploadok=1;
	
	$uploadok=checkImg($_FILES["file"]);
	// Check if $uploadOk is set to 0 by an error
	if ($uploadok == 0) {
		echo "<h3>Sorry, your file was not uploaded.</h3>";

		header("Refresh: 3; URL=../pages/createPoll.php");
	// if everything is ok, try to upload file
	} else {
		if (move_uploaded_file($_FILES["file"]["tmp_name"], $imgpath)) {
			echo "<h3>The file ". basename($_FILES["file"]["name"]). " has been uploaded.</h3>";
			newPoll($userid, $title, $imgpath);

			$pollid = getPollIDByTitle($title);

			if(isset($_POST["mytext"])){    
			    foreach($_POST["mytext"] as $key => $text_field){
			        if(!empty($text_field)){
			        	insertAnswer($pollid[0], $_SESSION['userid'], $text_field);
			    	}
			    }
			}

			print "<h3>Poll sussefully created!<h3>";
			print "<h3>Redirecting...<h3>";

			header("Refresh: 2; URL=../index.php");
		} else {
			echo "<h3>Sorry, there was an error uploading your file.</h3>";

			header("Refresh: 3; URL=../pages/createPoll.php");
		}
	}
?>