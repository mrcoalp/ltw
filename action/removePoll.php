<?php
	include_once('../database/polls.php');
	include_once('../database/answers.php');

	function remove($id){
        deletePoll($id);
        removeAnswersOfPoll($id);
    }

    if (isset($_POST['id'])) {
        remove($_POST['id']);
    }

?>