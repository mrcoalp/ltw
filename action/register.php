<?php
	session_start();

	include_once('../database/users.php');

	$username = $_POST["username"];
	$name = $_POST["name"];
	$email = $_POST["email"];
	$pass = $_POST["pwd"];

	$sha1pass = sha1($pass);
	$md5pass = md5($sha1pass);
	$cryptpass = crypt($md5pass, 'st');

	register($username, $name, $email, $cryptpass);

	print "<h3>User sussefully registered!<h3>";
	print "<h3>Redirecting...<h3>";

	$id = getIdByUser($username);
	$_SESSION['userid']=$id[0];

	$_SESSION['username']=$username;

	$name = getNameByUser($username);
	$_SESSION['name']=$name[0];
	header("Refresh: 2; URL=../index.php");
?>