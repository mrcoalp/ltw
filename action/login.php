<?php
	session_start();

	include_once('../database/users.php');

	$username = $_POST["username"];
	$pass = $_POST["pwd"];

	$array = getPassByUser($username);
	print "<h1>Wrong login! :(</h1>";
	print "<h2>Redirecting...</h2>";

	$sha1pass = sha1($pass);
	$md5pass = md5($sha1pass);
	$cryptpass = crypt($md5pass, 'st');

	if (empty($array)) {
		print '<script type="text/javascript">';
		print 'alert("Inexistent user")';
		print '</script>';
		header('Refresh: 1; URL=../index.php');
		//header("location: ../index.php");
		//echo "Utilizador nao existente";
		return;
	}
	else if ($array==false) {
		print '<script type="text/javascript">';
		print 'alert("Inexistent user")';
		print '</script>';
		header('Refresh: 1; URL=../index.php');
		//header("location: ../index.php");
		//echo "Utilizador nao existente";
		return;
	}
	else{
		if($array[0]==$cryptpass){
			$id = getIdByUser($username);
			$_SESSION['userid']=$id[0];

			$_SESSION['username']=$username;

			$name = getNameByUser($username);
			$_SESSION['name']=$name[0];
			
			header("Location: ../index.php");
		}
		else{
			print '<script type="text/javascript">';
			print 'alert("Wrong password")';
			print '</script>';
			header('Refresh: 1; URL=../index.php');
			//echo "Password errada";
			//header("location: ../index.php");
			return;
		}
	}
?>