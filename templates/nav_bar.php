   <?php session_start(); ?>

   <!-- Fixed navbar -->
    <div class="navbar navbar-default navbar-fixed-top" role="navigation">
      	<div class="container">
        	<div class="navbar-header">
          		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
	            	<span class="sr-only">Toggle navigation</span>
	            	<span class="icon-bar"></span>
	            	<span class="icon-bar"></span>
	            	<span class="icon-bar"></span>
          		</button>
          		<a class="navbar-brand">Polls Pool</a>
        	</div>
        	<div class="collapse navbar-collapse">
	          	<ul class="nav navbar-nav">
	           		<li class="active"><a href="../index.php">Home</a></li>
	            	<li><a href="about.php">About</a></li>
	          	</ul>
	      		<div class="nav navbar-nav navbar-right " >
	      			<? if(isset($_SESSION['username'])){?>
	      				<li class="dropdown">
						  <?php print '<a href="#" class="dropdown-toggle" data-toggle="dropdown"><b>'.$_SESSION['name'].'</b> <b class="caret"></b></a>'?>
						  <ul class="dropdown-menu">
							<?php print '<li><a href="'.$_SERVER['REQUEST_URI'].'../../../pages/mypolls.php">My polls</a></li>'?>
							<?php print '<li><a href="'.$_SERVER['REQUEST_URI'].'../../../action/logout.php">Logout</a></li>'?>
						  </ul>
						</li>
	      			<?}
	      			else{?>
		       			<form method="post" class="form-inline" role="form" action="../action/login.php">
				  			<div class="form-group">
						    	<label for="email">Username:</label>
						    	<input type="text" class="form-control" name="username">
						  	</div>
						  	<div class="form-group">
						    	<label for="pwd">Password:</label>
						    	<input type="password" class="form-control" name="pwd">
						  	</div>
						  	<button type="submit" class="btn btn-default">Login</button>
						</form>
					<?}?>
	    		</div>
	        </div>
	    </div>
	</div>	