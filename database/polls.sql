CREATE TABLE users(
	id INTEGER PRIMARY KEY AUTOINCREMENT,
	func VARCHAR,
	username VARCHAR,
	name VARCHAR,
	email VARCHAR,
	password VARCHAR,
	datemember DATETIME NOT NULL DEFAULT (date('now'))
);

CREATE TABLE polls(
	id INTEGER PRIMARY KEY AUTOINCREMENT,
	userID INTEGER REFERENCES users,
	title VARCHAR,
	imagePath VARCHAR,
	datePoll DATETIME NOT NULL DEFAULT (datetime('now'))
);

CREATE TABLE answers(
	id INTEGER PRIMARY KEY AUTOINCREMENT,
	pollID INTEGER REFERENCES polls,
	userID INTEGER REFERENCES users,
    textAnswer VARCHAR,
	dateAnswer DATETIME NOT NULL DEFAULT (datetime('now'))
);

CREATE TABLE votes(
	id INTEGER PRIMARY KEY AUTOINCREMENT,
	pollID INTEGER REFERENCES polls,
	userID INTEGER REFERENCES users,
    answerID INTEGER REFERENCES answers,
	dateVote DATETIME NOT NULL DEFAULT (datetime('now'))
);

INSERT INTO users (func, username, name, email, password) 
VALUES ("admin", "mrcoalp", "Marco António", "marco.alp120@gmail.com", "stnoNZ40oY4Fc");

INSERT INTO users (func, username, name, email, password) 
VALUES ("user", "bernardomfg", "Bernardo Gonçalves", "bernardomfg@gmail.com", "stzTP1/CqXH8A");

INSERT INTO polls (userID, title, imagePath) VALUES (1, "Is the planet round?", "../images/planet.jpg");

INSERT INTO polls (userID, title, imagePath) VALUES (2, "Windows or Linux?", "../images/wu.png");

INSERT INTO polls (userID, title, imagePath) VALUES (1, "All answers?", "../images/feup.png");

INSERT INTO answers (pollID, userID, textAnswer) VALUES (1, 1, "Yes");
INSERT INTO answers (pollID, userID, textAnswer) VALUES (1, 1, "No");

INSERT INTO answers (pollID, userID, textAnswer) VALUES (2, 2, "Windows");
INSERT INTO answers (pollID, userID, textAnswer) VALUES (2, 2, "Linux");

INSERT INTO answers (pollID, userID, textAnswer) VALUES (3, 1, "1 answer");
INSERT INTO answers (pollID, userID, textAnswer) VALUES (3, 1, "2 answer");
INSERT INTO answers (pollID, userID, textAnswer) VALUES (3, 1, "3 answer");
INSERT INTO answers (pollID, userID, textAnswer) VALUES (3, 1, "4 answer");
INSERT INTO answers (pollID, userID, textAnswer) VALUES (3, 1, "5 answer");
INSERT INTO answers (pollID, userID, textAnswer) VALUES (3, 1, "6 answer");
INSERT INTO answers (pollID, userID, textAnswer) VALUES (3, 1, "7 answer");
INSERT INTO answers (pollID, userID, textAnswer) VALUES (3, 1, "8 answer");
INSERT INTO answers (pollID, userID, textAnswer) VALUES (3, 1, "9 answer");
INSERT INTO answers (pollID, userID, textAnswer) VALUES (3, 1, "10 answer");

