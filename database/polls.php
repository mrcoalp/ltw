<?php
	include_once('connection.php');

	function newPoll($userid, $title, $imgpath){
		global $db;

		$stmt = $db->prepare("INSERT INTO polls (userID, title, imagePath) VALUES ('$userid', '$title', '$imgpath')");
		$stmt->execute();
	}

	function getPollsLimit(){
		global $db;

		$stmt = $db->prepare("SELECT * FROM polls ORDER BY datePoll DESC LIMIT 3");
		$stmt->execute();
		$result = $stmt->fetchAll();

  		return $result;
	}

	function getPollIDByTitle($title){
		global $db;

		$stmt = $db->query("SELECT id FROM polls WHERE title = '$title'");
		$result = $stmt->fetch();

  		return $result;
	}

	function getPollsByUser($id){
		global $db;

		$stmt = $db->prepare("SELECT * FROM polls WHERE userID = '$id'");
		$stmt->execute();
		$result = $stmt->fetchAll();

  		return $result;
	}

	function deletePoll($id){
		global $db;

		$stmt = $db->prepare("DELETE FROM polls WHERE id='$id'");
		$stmt->execute();
	}

	function getTitleByPollID($id){
		global $db;

		$stmt = $db->query("SELECT title FROM polls WHERE id = '$id'");
		$result = $stmt->fetch();

  		return $result;
	}
	
	
	function getUserIDByPollID($id){
		global $db;

		$stmt = $db->query("SELECT userID FROM polls WHERE id = '$id'");
		$result = $stmt->fetch();

  		return $result;
	}

	function getAllPolls(){
		global $db;

		$stmt = $db->prepare("SELECT * FROM polls ORDER BY datePoll DESC");
		$stmt->execute();
		$result = $stmt->fetchAll();

  		return $result;
	}

	function searchPollByTitle($title){
		global $db;

		$stmt = $db->prepare("SELECT * FROM polls WHERE title LIKE '%$title%'");
		$stmt->execute();
		$result = $stmt->fetchAll();

  		return $result;
	}

	function searchPollByUsername($username){
		global $db;

		$stmt = $db->prepare("SELECT * FROM polls WHERE userID=(SELECT id FROM users WHERE username LIKE '%$username%')");
		$stmt->execute();
		$result = $stmt->fetchAll();

  		return $result;
	}

	function searchPollByName($name){
		global $db;

		$stmt = $db->prepare("SELECT * FROM polls WHERE userID=(SELECT id FROM users WHERE name LIKE '%$name%')");
		$stmt->execute();
		$result = $stmt->fetchAll();

  		return $result;
	}

	function getImagePath($id){
		global $db;

		$stmt = $db->query("SELECT imagePath FROM polls WHERE id = '$id'");
		$result = $stmt->fetch();

  		return $result;
	}
?>