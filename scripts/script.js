function logout() {
    window.location = "../action/logout.php";
  	return false;
}

function createPoll(){
	window.location = "../pages/createPoll.php";
  	return false;
}

/*function addAnswer(){
	var form = document.getElementById("answers");
	var label = document.createElement("label");

	var att = document.createAttribute("for");
	att.value = "answer";
	label.setAttributeNode(att);

	var t = document.createTextNode("Answer");       
	label.appendChild(t);                              
	form.appendChild(label);

	var input = document.createElement("input");

	var att2 = document.createAttribute("type");
	att2.value = "text";
	input.setAttributeNode(att2);

	var att3 = document.createAttribute("class");
	att3.value = "form-control";
	input.setAttributeNode(att3);

	var att4 = document.createAttribute("name");
	att4.value = "answer";
	input.setAttributeNode(att4);

	form.appendChild(input);
}*/

$(document).ready(function() {
    var max_fields      = 10; //maximum input boxes allowed
    var wrapper         = $(".input_fields_wrap"); //Fields wrapper
    var add_button      = $(".add_field_button"); //Add button ID
    
    var x = 1; //initlal text box count
    $(add_button).click(function(e){ //on add input button click
        e.preventDefault();
        if(x < max_fields){ //max input box allowed
            x++; //text box increment
            $(wrapper).append('<div class="input-group"><input type="text" class="form-control" name="mytext[]"/><span class="input-group-btn"><a href="#" class="btn btn-default remove_field">Remove</a></span></div>'); //add input box
        }
    });
    
    $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
        e.preventDefault();
        $(this).parents("div:first").remove();
        x--;
    })
});

$('.removepoll').on('click', function(){
	var id = $(this).attr('id');

	$.ajax({
		url: '../action/removePoll.php',
		type: 'post',
		data: { "id": id},
		success: function(response) { 
			window.location='mypolls.php';
		}
	});
});

$('.viewform').on('click', function(){
	$(this).closest('form').submit();
});

function isEmpty(str) {
    return (!str || 0 === str.length);
}


$(document).ready(function() {
	$('.btneditanswer').on('click', function(){
		var id = $(this).closest('.divanswers').children('h3').attr('idanswer');
		$(this).closest('form').children('button').prop('disabled', false);
		$(this).closest('.divanswers').append('<div class="input-group"><input type="hidden" class="form-control" name="myid[]" value="'+id+'"/><input type="text" class="form-control" name="mytext[]"/><span class="input-group-btn"><button class="btn btn-default cancelanswer">Cancel</button></span></div>');
		$(this).prop('disabled', true);
	});
});

$(document).on('click', '.cancelanswer', function(e) {
   	e.preventDefault();
   	$(this).closest('.divanswers').children('h3').children('.btneditanswer').prop('disabled', false);
   	$(this).closest('.input-group').remove();
   	if(isEmpty($('.input-group'))){
   		$('form').children('button').prop('disabled', true);
   	}
   	return false;
});

$('.btnvote').on('click', function(){
	var userid = $(this).attr('userid');
	var pollid = $(this).attr('pollid');
	var answerid = $(this).attr('answerid');

	$.ajax({
		url: '../action/vote.php',
		type: 'post',
		data: { 'userid': userid, 'pollid': pollid, 'answerid': answerid},
		success: function(response) { 
			window.location='home.php';
		}
	});
});

function listPolls(){
	window.location = "../pages/allPolls.php";
  	return false;
}