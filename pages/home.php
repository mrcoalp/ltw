<!DOCTYPE html>
<html lang="en">
	<head>
    	<meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
    	<meta name="description" content="">
	    <meta name="author" content="">
	    <link rel="shortcut icon" href="../../assets/ico/favicon.ico">

    	<title>Polls Pool</title>

	    <!-- Bootstrap core CSS -->
	    <link href="../bootstrap/css/bootstrap.min.css" rel="stylesheet">

	    <!-- Custom styles for this template -->
	    <link href="../css/style.css" rel="stylesheet">

	    <!-- Just for debugging purposes. Don't actually copy this line! -->
	    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

	    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	    <!--[if lt IE 9]>
	      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	    <![endif]-->
  	</head>

  	<body>

		<?php include('../templates/nav_bar.php');
			if(!isset($_SESSION['username'])){
				header("Location: ../index.php");
			}
		?>
		<div class="container">
			<?php print '<h1>Polls pool</h1>';?>
			<button type="button" onclick="createPoll()" class="btn btn-default"><h4>Create new poll</h4></button>
			<br>
			<div class="row">
				<div class="col-lg-2">
					<button type="button" onclick="listPolls()" class="btn btn-default"><h4>List all polls</h4></button>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-6">
					<form method="post" class="form" role="form" action="../pages/searchPoll.php">
						<div class="input-group">
							<input type="text" name="search" class="form-control">
							<span class="input-group-btn">
								<button type="submit" class="btn btn-default searchpoll">Search poll</button>
							</span>
						</div>
					</form>
				</div>
			</div>
		</div>
		<div class="container">
			<h2>Last polls:</h2>
			<div class="row">
				<div class="col-lg-1">
				</div>
				<div class="col-lg-11">
					<?php include('../templates/listPolls.php');?>
				</div>
			</div>
		</div>

		<?php include('../templates/footer.php');?>
	</body>
</html>