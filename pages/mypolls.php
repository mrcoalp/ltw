<!DOCTYPE html>
<html lang="en">
	<head>
    	<meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
    	<meta name="description" content="">
	    <meta name="author" content="">
	    <link rel="shortcut icon" href="../../assets/ico/favicon.ico">

    	<title>Polls Pool</title>

	    <!-- Bootstrap core CSS -->
	    <link href="../bootstrap/css/bootstrap.min.css" rel="stylesheet">

	    <!-- Custom styles for this template -->
	    <link href="../css/style.css" rel="stylesheet">

	    <!-- Just for debugging purposes. Don't actually copy this line! -->
	    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

	    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	    <!--[if lt IE 9]>
	      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	    <![endif]-->
  	</head>

  	<body>

		<?include('../templates/nav_bar.php');
			if(!isset($_SESSION['username'])){
				header("Location: ../index.php");
			}
		?>

		<div class="container">

			<?php
				include_once('../database/polls.php');

				print '<h2>My polls</h2><br>';

				$polls = getPollsByUser($_SESSION['userid']);

				if(empty($polls)){
					echo '<h3>No polls created...</h3>';
				}
				else{
					foreach( $polls as $row) {
						echo '<form id= "pollform" action="viewPoll.php" method="post" >';
							echo '<input type="hidden" name="pollid" value="'.$row['id'].'">';
							echo '<a class="viewform" href="#"><h3>' . $row['title'] . '</h3></a>';
						echo '</form>';
					    echo '<form id= "pollform" action="editPoll.php" method="post" >';
					    	echo '	'.$row['datePoll'];
					    	echo '	';
					    	echo '<input type="hidden" name="pollid" value="'.$row['id'].'">';
						 	echo '<button type="submit" class="btn btn-default">Edit</button>';
						 	echo '<button type="button" class="removepoll btn btn-default" id="'.$row['id'].'">Remove</button>';
						echo '</form>';
				  	}
			  	}
			?>
			<br>
			<br>
			<br>
			<button type="button" onclick="createPoll()" class="btn btn-default"><h4>Create new poll</h4></button>
		</div>

		<?include('../templates/footer.php');?>
	</body>
</html>