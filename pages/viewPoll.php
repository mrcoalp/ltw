<!DOCTYPE html>
<html lang="en">
	<head>
    	<meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
    	<meta name="description" content="">
	    <meta name="author" content="">
	    <link rel="shortcut icon" href="../../assets/ico/favicon.ico">

    	<title>Polls Pool</title>

	    <!-- Bootstrap core CSS -->
	    <link href="../bootstrap/css/bootstrap.min.css" rel="stylesheet">

	    <!-- Custom styles for this template -->
	    <link href="../css/style.css" rel="stylesheet">

	    <!-- Just for debugging purposes. Don't actually copy this line! -->
	    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

	    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	    <!--[if lt IE 9]>
	      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	    <![endif]-->
  	</head>

  	<body>

		<?include('../templates/nav_bar.php');
			if(!isset($_SESSION['username'])){
				header("Location: ../index.php");
			}
		?>
		<div class="container">
			<?php
				include("../database/answers.php");
				include("../database/polls.php");
				include("../database/votes.php");

				if(isset($_POST['pollid'])){
			    	$pid = $_POST['pollid'];
			    	$answers = getAnswersByPoll($pid);

			    	$title = getTitleByPollID($pid);
			    	$image = getImagePath($pid);

			    	echo '<div class="row">';
						echo '<div class="col-lg-4">';
							echo '<img src="'.$image[0].'" id="imgpoll">';
						echo '</div>';
						echo '<div class="col-lg-8">';
							echo '<h2><b>' . $title[0] . '</b></h2>';
							foreach( $answers as $row) {
								$votes=getVotesOfAnswer($row['id']);
								if(getUserIDByPollID($pid)[0] != $_SESSION['userid']){
									$check = checkVotes($_SESSION['userid'], $pid);
									if(empty($check[0])){
										echo '<h3>- ' . $row['textAnswer'];
										echo '	<button class="btn btn-default btnvote" userid="'.$_SESSION['userid'].'" pollid="'.$pid.'" answerid="'.$row['id'].'">Vote</button>';
										echo '</h3>';
									}
									else{
										echo '<h3>- ' . $row['textAnswer'] . ' || '.$votes[0].' votes';
										echo '	<button class="btn btn-default btnvote" userid="'.$_SESSION['userid'].'" pollid="'.$pid.'" answerid="'.$row['id'].'" disabled>Vote</button>';
										echo '</h3>';
									}
								}
								else{
									echo '<h3>- ' . $row['textAnswer'] . ' || '.$votes[0].' votes';
									echo '</h3>';
								}
						  	}
						echo '</div>';
					echo '</div>';
				}
			?>
		</div>

		<?include('../templates/footer.php');?>
	</body>
</html>