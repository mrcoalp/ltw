<!DOCTYPE html>
<html lang="en">
	<head>
    	<meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
    	<meta name="description" content="">
	    <meta name="author" content="">
	    <link rel="shortcut icon" href="../../assets/ico/favicon.ico">

    	<title>Polls Pool</title>

	    <!-- Bootstrap core CSS -->
	    <link href="../bootstrap/css/bootstrap.min.css" rel="stylesheet">

	    <!-- Custom styles for this template -->
	    <link href="../css/style.css" rel="stylesheet">

	    <!-- Just for debugging purposes. Don't actually copy this line! -->
	    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

	    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	    <!--[if lt IE 9]>
	      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	    <![endif]-->
  	</head>

  	<body>

		<?include('../templates/nav_bar.php');
			if(!isset($_SESSION['username'])){
				header("Location: ../index.php");
			}
		?>
		<div class="container" id="aboutpage">
			<h1><b>LTW</b></h1>
			<br>
			<img src="../images/feup.png" id="imgpoll">
			<h2>Mestrado Integrado em Engenharia Informática e Computação</h2>
			<h3>2014/2015</h3>
			<br>
			<br>
			<h3>Grupo:</h3>
			<h4>Marco António Lopes Pinto - 201100706</h3>
			<h4>Bernardo de Matos Fernandes Gonçalves - 201109255</h3>
			<br>
			<h4>Faculdade de Engenharia da Universidade do Porto Rua Roberto Frias, sn, 4200-465 Porto, Portugal</h3>

		</div>

		<?include('../templates/footer.php');?>
	</body>
</html>