<!DOCTYPE html>
<html lang="en">
	<head>
    	<meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
    	<meta name="description" content="">
	    <meta name="author" content="">
	    <link rel="shortcut icon" href="../../assets/ico/favicon.ico">

    	<title>Polls Pool</title>

	    <!-- Bootstrap core CSS -->
	    <link href="../bootstrap/css/bootstrap.min.css" rel="stylesheet">

	    <!-- Custom styles for this template -->
	    <link href="../css/style.css" rel="stylesheet">

	    <!-- Just for debugging purposes. Don't actually copy this line! -->
	    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

	    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	    <!--[if lt IE 9]>
	      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	    <![endif]-->
  	</head>

  	<body>

		<?php include('../templates/nav_bar.php');?>

		<?php if(isset($_SESSION['username'])){
			header('Location: home.php');
  		}?>

		<!-- Begin page content -->
		<div class="container">
			<div class="row">
				<div class="col-lg-3"></div>
				<div class="col-lg-6" id="cenas">
		    		<div class="page-header">
		    			<h1>Polls Pool</h1>
					</div>
					<p class="lead">New user? Register...</p>
				    <form method="post" role="form" action="../action/register.php">
				    	<div class="form-group">
					    	<label for="username">Username:</label>
					    	<input type="text" class="form-control" name="username">
					  	</div>
					  	<div class="form-group">
					    	<label for="username">Name:</label>
					    	<input type="text" class="form-control" name="name">
					  	</div>
					  	<div class="form-group">
					    	<label for="email">Email address:</label>
					    	<input type="email" class="form-control" name="email">
					  	</div>
					  	<div class="form-group">
						    <label for="pwd">Password:</label>
						    <input type="password" class="form-control" name="pwd">
					  	</div>
					  	<button type="submit" class="btn btn-default">Register</button>
					</form>
					<p></p>
			</div>
			<div class="col-lg-3">
		
	</div>
		</div>
	</div>


		<?php include('../templates/footer.php');?>
	</body>
</html>