<!DOCTYPE html>
<html lang="en">
	<head>
    	<meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
    	<meta name="description" content="">
	    <meta name="author" content="">
	    <link rel="shortcut icon" href="../../assets/ico/favicon.ico">

    	<title>Polls Pool</title>

	    <!-- Bootstrap core CSS -->
	    <link href="../bootstrap/css/bootstrap.min.css" rel="stylesheet">

	    <!-- Custom styles for this template -->
	    <link href="../css/style.css" rel="stylesheet">

	    <!-- Just for debugging purposes. Don't actually copy this line! -->
	    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

	    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	    <!--[if lt IE 9]>
	      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	    <![endif]-->
  	</head>

  	<body>

		<?include('../templates/nav_bar.php');
			if(!isset($_SESSION['username'])){
				header("Location: ../index.php");
			}
		?>
		<div class="container">
			<?php
				include_once('../database/answers.php');
				include_once('../database/polls.php');

				if(isset($_POST['pollid'])){
			    	$pid = $_POST['pollid'];
			    	echo '<h2>Edit Poll: ';

			    	$poll = getTitleByPollID($pid);
			    	echo '"'.$poll[0].'"</h2>';

					$answers = getAnswersByPoll($pid);

					echo '<form method="post" class="form" role="form" action="../action/updatePoll.php">';

					foreach( $answers as $row) {
						echo '<div class="divanswers"><h3 class="answer" idanswer="'.$row['id'].'">' . $row['textAnswer'];
						echo '	<button class="btn btn-default btneditanswer">Edit</button></h3></div>';
		  			}
				}
			?>
				<button type="submit" class="btn btn-default" disabled>Update poll</button>
			</form>
		</div>

		<?include('../templates/footer.php');?>
	</body>
</html>
