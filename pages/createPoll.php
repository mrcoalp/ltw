<!DOCTYPE html>
<html lang="en">
	<head>
    	<meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
    	<meta name="description" content="">
	    <meta name="author" content="">
	    <link rel="shortcut icon" href="../../assets/ico/favicon.ico">

    	<title>Polls Pool</title>

	    <!-- Bootstrap core CSS -->
	    <link href="../bootstrap/css/bootstrap.min.css" rel="stylesheet">

	    <!-- Custom styles for this template -->
	    <link href="../css/style.css" rel="stylesheet">

	    <!-- Just for debugging purposes. Don't actually copy this line! -->
	    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

	    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	    <!--[if lt IE 9]>
	      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	    <![endif]-->
  	</head>

  	<body>

		<?include('../templates/nav_bar.php');
			if(!isset($_SESSION['username'])){
				header("Location: ../index.php");
			}
		?>
		<div class="container">
			<? //print '<h1>'.$_SESSION['name']."'s ".'profile</h1>';?>
			<h2>Poll creation</h2>
			<form method="post" class="form" role="form" action="../action/createPoll.php" enctype="multipart/form-data">
				<div class="form-group">
					<label for="question">Question?</label>
					<input type="text" class="form-control" name="question">
				</div>
				<div class="form-group">
					<label for="lblImage">Image for Poll</label>
					<input type="file" id="fileUploadForm" name="file">
					<p class="help-block">Select an image to upload (max size 2MB)</p>
				</div>
				<div class="row">
					<div class="col-lg-8">
						<button class="btn btn-default add_field_button">Add Answer (max 10)</button>
						<div class="input_fields_wrap form-group">
							<input type="text" class="form-control" name="mytext[]">
						</div>
					</div>
					<div class="col-lg-2">
					</div>
					<div class="col-lg-2">
					</div>
				</div>
    				<!--<div><input type="text" class="form-control" name="mytext[]"></div>-->
				<!--<button type="button" onclick="addAnswer()" class="btn btn-default">Add answer</button>-->
				<button type="submit" class="btn btn-default">Create poll</button>
			</form>
		</div>

		<?include('../templates/footer.php');?>
	</body>
</html>